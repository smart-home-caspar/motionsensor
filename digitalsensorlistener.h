#ifndef DIGITALSENSORLISTENER_H
#define DIGITALSENSORLISTENER_H

class DigitalSensorListener {
  public:
    virtual void update(bool newState) = 0;
};

#endif