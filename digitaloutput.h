#ifndef DIGITALOUTPUT_H
#define DIGITALOUTPUT_H

class DigitalOutput {
  private:
    int m_pin;

  public:
    DigitalOutput(int pin)
     : m_pin(pin) {
      pinMode(m_pin, OUTPUT);
    }

    bool isOn() {
      return digitalRead(m_pin);
    }

    void turnOn() {
      digitalWrite(m_pin, true);
    }

    void turnOff() {
      digitalWrite(m_pin, false);
    }
};

#endif