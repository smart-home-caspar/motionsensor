#ifndef MAIN_H
#define MAIN_H

#include "webserver.h"
#include "digitalinput.h"
#include "motionsensor.h"
#include "analoginput.h"
#include "thresholdrelay.h"
#include "mqttclient.h"
#include "digitalsensorpublisher.h"
#include "digitaloutput.h"
#include "statusledcontroller.h"

const char* ssid = "Vanemuine";
const char* password = "D6D70263BD";
const char* host = "MOTION_SENSOR";

const char* mqttServer = "powerhouse";
const int mqttPort = 1883;

const char* motionSensorPublishTopic = "home/motion";
const char* lightSensorPublishTopic = "home/light";

const int motionSensorPin = 4;
const int lightSensorPin = A0;
const int statusLedPin = 5;

const int lightSensorThreshold = 650;
const int lightSensorReadIntervalMillis = 60*1000;
const int lightSensorHysteresisUpDelta = 200;
const int lightSensorHysteresisDownDelta = 200;

WebServer webServer;

MqttClient mqttClient;

MotionSensor motionSensor(new DigitalInput(motionSensorPin));
DigitalSensorPublisher motionSensorPublisher(&mqttClient, motionSensorPublishTopic);

ThresholdRelay lightSensor(new AnalogInput(lightSensorPin, lightSensorReadIntervalMillis), lightSensorThreshold);
Hysteresis lightSensorHysteresis(lightSensorHysteresisUpDelta, lightSensorHysteresisDownDelta);
DigitalSensorPublisher lightSensorPublisher(&mqttClient, lightSensorPublishTopic);
StatusLedController* statusLedController;

void setup(void) {
  Serial.begin(74880);
  printResetInfo();
  webServer.start(ssid, password, host);
  mqttClient.start(mqttServer, mqttPort);
  motionSensor.addListener(&motionSensorPublisher);
  lightSensor.addListener(&lightSensorPublisher);
  lightSensor.setHysteresis(lightSensorHysteresis);
  statusLedController = new StatusLedController(new DigitalOutput(statusLedPin));
  motionSensor.addListener(statusLedController);
}

void loop(void)
{
  noInterrupts();
  while(1) {
    ESP.wdtFeed();
    webServer.service();
    motionSensor.service();
    lightSensor.service();
    mqttClient.service();
    if (!statusLedController->isSleeping()) {
      statusLedController->run();
    }
    delay(0);
  }
}

void printResetInfo()
{
  rst_info* rtc_info = ESP.getResetInfoPtr();
  Serial.print("reset reason: ");
  Serial.println(rtc_info->reason);
  if (rtc_info->reason == REASON_WDT_RST || rtc_info->reason == REASON_EXCEPTION_RST || rtc_info->reason == REASON_SOFT_WDT_RST) {
    if (rtc_info->reason == REASON_EXCEPTION_RST) {
      Serial.print("Fatal exception: ");
      Serial.println(rtc_info->exccause);
    }
    Serial.printf("epc1=0x%08x, epc2=0x%08x, epc3=0x%08x, excvaddr=0x%08x, depc=0x%08x\n", rtc_info->epc1, rtc_info->epc2, rtc_info->epc3, rtc_info->excvaddr, rtc_info->depc);//The address of the last crash is printed, which is used to debug garbled output.
  }
}

#endif