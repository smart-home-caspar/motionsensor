#ifndef HYSTERESIS_H
#define HYSTERESIS_H

class Hysteresis {
  private:
    int m_upDelta;
    int m_downDelta;

  public:
    Hysteresis(int upDelta, int downDelta)
     : m_upDelta(upDelta)
     , m_downDelta(downDelta) {
    }

    bool process(int value, int threshold, bool currentState) {
      if (currentState) {
        return value >= (threshold - m_downDelta);
      } else {
        return value > (threshold + m_upDelta);
      }
    }

};

#endif