#ifndef DIGITALSENSOR_H
#define DIGITALSENSOR_H

#include "digitalsensorlistener.h"

#include <vector>

class DigitalSensor {
  private:
    std::vector<DigitalSensorListener*> m_listeners;

  public:
    void addListener(DigitalSensorListener* listener) {
      m_listeners.push_back(listener);
    }

  protected:
    virtual ~DigitalSensor() {}

    void updateListeners(bool newState) {
      for (auto listener : m_listeners) {
        listener->update(newState);
      }
    }

};

#endif