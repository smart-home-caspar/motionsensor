#ifndef THRESHOLDRELAY_H
#define THRESHOLDRELAY_H

#include "digitalsensor.h"
#include "analoginput.h"
#include "hysteresis.h"

class ThresholdRelay : public DigitalSensor {
  private:
    AnalogInput* m_input;
    int m_threshold;
    bool m_currentState;
    Hysteresis m_hysteresis;

  public:
    ThresholdRelay(AnalogInput* input, int threshold)
     : m_input(input)
     , m_threshold(threshold)
     , m_hysteresis(0, 0) {
      updateCurrentState();
    }

    void setHysteresis(Hysteresis& hysteresis) {
      m_hysteresis = hysteresis;
    }

    void service() {
      auto previousState = m_currentState;
      updateCurrentState();
      if (m_currentState != previousState) {
        updateListeners(m_currentState);
      }
    }

  private:
    void updateCurrentState() {
      m_currentState = m_hysteresis.process(read(), m_threshold, m_currentState);
    }

    int read() {
      return m_input->read();
    }

};

#endif