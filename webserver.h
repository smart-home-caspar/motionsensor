#ifndef WEBSERVER_H
#define WEBSERVER_H

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <ESP8266HTTPUpdateServer.h>

class WebServer {
  private:
    ESP8266WebServer m_httpServer;
    ESP8266HTTPUpdateServer m_httpUpdater;
  public:

    WebServer()
     : m_httpServer(80) {
    }

    void start(const char* ssid, const char* password, const char* host) {
      WiFi.setPhyMode(WIFI_PHY_MODE_11G);
      WiFi.setOutputPower(2.5);
      WiFi.mode(WIFI_STA);
      WiFi.begin(ssid, password);

      while (WiFi.waitForConnectResult() != WL_CONNECTED) {
        WiFi.begin(ssid, password);
        Serial.println("WiFi failed, retrying.");
      }

      m_httpUpdater.setup(&m_httpServer);
      m_httpServer.begin();
      MDNS.begin(host);

      Serial.println("HTTPUpdateServer ready");
      Serial.print("IP address: ");
      Serial.println(WiFi.localIP());
    }

    void service() {
      m_httpServer.handleClient();
      MDNS.update();
    }

};

#endif