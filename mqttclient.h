#ifndef MQTTCLIENT_H
#define MQTTCLIENT_H

#include <ESP8266WiFi.h>
#include <PubSubClient.h>

#include <string>

class MqttClient {
  private:
    WiFiClient m_wifi;
    PubSubClient m_client;

  public:
    MqttClient()
     : m_client(m_wifi) {
    }

    void start(const char* server, int port) {
      if (WiFi.status() != WL_CONNECTED) {
        Serial.println("Can not start mqtt client - wifi is not connected.");
      }

      connect(server, port);
    }

    void service() {
      m_client.loop();
    }

    void subscribe(const char* topic) {
      m_client.subscribe(topic);
    }

    void publish(const char* topic, const char* message) {
      auto success = m_client.publish(topic, message);
      printPublishInfoToSerial(success, message, topic);
    }

  private:
    void connect(const char* server, int port) {
      m_client.setServer(server, port);
      while (!m_client.connected()) {
        Serial.println("Connecting to MQTT...");
        if (m_client.connect("ESP8266Client")) {
          Serial.println("connected");
        } else {
          Serial.print("failed with state ");
          Serial.print(m_client.state());
          delay(2000);
        }
      }
    }

    void printPublishInfoToSerial(bool success, const char* topic, const char* message) {
      if (success) {
        Serial.print("Successfully published '");
      } else {
        Serial.print("Failed to publish '");
      }
      Serial.print(message);
      Serial.print("' to topic '");
      Serial.print(topic);
      Serial.println("'");
    }

};

#endif