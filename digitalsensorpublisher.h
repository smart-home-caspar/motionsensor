#ifndef DIGITALSENSORPUBLISHER_H
#define DIGITALSENSORPUBLISHER_H

#include "digitalsensorlistener.h"
#include "mqttclient.h"

#include <string>

using namespace std;

class DigitalSensorPublisher : public DigitalSensorListener {
  private:
    MqttClient* m_mqtt;
    const char* m_topic;

  public:
    DigitalSensorPublisher(MqttClient* mqtt, const char* topic)
     : m_mqtt(mqtt)
     , m_topic(topic) {
    }

    void update(bool newState) override {
      m_mqtt->publish(m_topic, buildMessage(newState));
    }

  private:
    const char* buildMessage(bool newState) {
        return (string("Sensor state changed to ") + string(newState ? "true" : "false")).c_str();
    }
};

#endif