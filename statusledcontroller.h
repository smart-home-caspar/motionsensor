#ifndef STATUSLEDCONTROLLER_H
#define STATUSLEDCONTROLLER_H

#include "digitaloutput.h"
#include "iprocess.h"
#include "digitalsensorlistener.h"

class StatusLedController : public DigitalSensorListener, public IProcess {
  private:
    DigitalOutput* m_led;
    IProcess* m_process;

  public:
    StatusLedController(DigitalOutput* led)
     : m_led(led)
     , m_process(new StartBlinkProcess(this, 3, 70)) {}

    void update(bool state) override {
      if (state) {
        switchToOnProcess();
      } else {
        switchToStandByProcess();
      }
    }

    bool isSleeping() override {
      return m_process->isSleeping();
    }

    void run() override {
      m_process->run();
    }

  private:
    void turnOff() {
      m_led->turnOff();
    }

    void turnOn() {
      m_led->turnOn();
    }

    bool isOn() {
      return m_led->isOn();
    }

    bool isOff() {
      return !isOn();
    }

    void switchToStandByProcess() {
      delete m_process;
      m_process = new StandByProcess(this, 10, 15*1000);
    }

    void switchToOnProcess() {
      delete m_process;
      m_process = new OnProcess(this);
    }

    class StartBlinkProcess : public IProcess {
      private:
        StatusLedController* m_ledController;
        int m_blinks;
        int m_pause;
        int m_currentBlink;

      public:
        StartBlinkProcess(StatusLedController* ledController, int blinks, int pause)
         : m_ledController(ledController)
         , m_blinks(blinks)
         , m_pause(pause)
         , m_currentBlink(0) {
           m_ledController->turnOff();
         }

        void run() override {
          if (m_currentBlink >= m_blinks) {
            m_ledController->switchToStandByProcess();
          } else if (m_ledController->isOff()) {
            m_ledController->turnOn();
            sleepFor(m_pause);
          } else {
            m_ledController->turnOff();
            m_currentBlink++;
            sleepFor(m_pause);
          }
        }
    };

    class StandByProcess : public IProcess {
      private:
        StatusLedController* m_ledController;
        int m_onMillis;
        int m_offMillis;

      public:
        StandByProcess(StatusLedController* ledController, int onMillis, int offMillis)
         : m_ledController(ledController)
         , m_onMillis(onMillis)
         , m_offMillis(offMillis) {
           m_ledController->turnOff();
           sleepFor(m_offMillis);
         }

        void run() override {
          if (m_ledController->isOff()) {
            m_ledController->turnOn();
            sleepFor(m_onMillis);
          } else {
            m_ledController->turnOff();
            sleepFor(m_offMillis);
          }
        }
    };

    class OnProcess : public IProcess {
      public:
        OnProcess(StatusLedController* ledController) {
           ledController->turnOn();
         }

        bool isSleeping() override {
          return true;
        }

        void run() override {
        }
    };

};

#endif