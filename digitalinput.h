#ifndef DIGITALINPUT_H
#define DIGITALINPUT_H

class DigitalInput {
  private:
    int m_pin;

  public:
    DigitalInput(int pin)
     : m_pin(pin) {
      pinMode(m_pin, INPUT);
    }

    bool read() {
      return digitalRead(m_pin);
    }
};

#endif