#ifndef MOTIONSENSOR_H
#define MOTIONSENSOR_H

#include "digitalsensor.h"
#include "digitalinput.h"

class MotionSensor : public DigitalSensor {
  private:
    DigitalInput* m_input;
    bool m_currentState;

  public:
    MotionSensor(DigitalInput* input)
     : m_input(input) {
      updateCurrentState();
    }

    void service() {
      auto previousState = m_currentState;
      updateCurrentState();
      if (m_currentState != previousState) {
        updateListeners(m_currentState);
      }
    }

  private:
    void updateCurrentState() {
      m_currentState = read();
    }

    bool read() {
      return m_input->read();
    }

};

#endif