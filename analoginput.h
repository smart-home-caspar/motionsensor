#ifndef ANALOGINPUT_H
#define ANALOGINPUT_H

class AnalogInput {
  private:
    int m_pin;
    int m_interval;
    int m_value;
    int m_previousReadMillis;

  public:
    AnalogInput(int pin, int intervalMillis)
     : m_pin(pin)
     , m_interval(intervalMillis) {
      pinMode(m_pin, INPUT);
      updateValue();
    }

    int read() {
      if (shouldUpdate()) {
        updateValue();
      }
      return m_value;
    }

  private:
    void updateValue() {
      m_value = analogRead(m_pin);
      m_previousReadMillis = millis();
    }

    bool shouldUpdate() {
      return millis() > (m_previousReadMillis + m_interval);
    }
};

#endif